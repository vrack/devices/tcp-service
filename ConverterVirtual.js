const net = require('net')
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port
module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('buffer').input().data('Buffer')
        .description('Буфер для отправки клиенту'),
      new Port('buffer').output().data('Buffer')
        .description('Буфер полученый от клиента')
    ]
  }

  checkParams () {
    return [
      new Rule('timeout').required().default(10000).isInteger().expression('value >= 0')
        .description('Таймаут отключения клиента от бездействия'),
      new Rule('port').required().default(4001).isInteger().expression('value >= 0')
        .description('Порт сервера')
    ]
  }

  settings () {
    return {
      message: false,
      shares: true
    }
  }

  shares = {
    connected: false,
    byteReceive: 0,
    byteSend: 0,
    sessions: 0
  }

  #server = {};
  #client = false;
  #timer = false;

  process () {
    this.#server = net.createServer((client) => {
      this.shares.connected = true
      if (this.#client) client.end()
      this.#client = client
      this.#client.on('end', () => {
        this.#client = false
        this.shares.connected = false
      })
      this.#client.on('data', (data) => {
        this.shares.byteReceive += data.length
        this.outputs.buffer.push(data)
      })
      this.#client.on('error', (error) => {
        this.error('Error on client', error)
      })
    })
    this.#server.listen(this.params.port, () => {})

    setInterval(() => { this.render() }, 5000)
  }

  inputBuffer (data) {
    this.shares.byteSend += data.length
    if (this.#client) this.#client.write(data)
  }
}
