Устройство поднимает сервер, и ждет подключение от внешних источников. После подключения, все данные которые приходят с внешних источников, он пробрасывает на свой выход `buffer`, и все что ему приходит на вход `buffer` он отправляет на внешние источники 

Данное устройство можно исопльзовать если есть необходимость создать стенд с виртуальными устройствами, которые работают через преобразователи типа Ethernet to CAN/RS485/RS232. 

Обычно используется с `basic.Mixer` если есть необходимость поднять виртуальную CAN/RS485 сеть.

Пример использования (только связи):

```json
"connections": [
    "ConverterServer.buffer -> Device1.buffer",
    "ConverterServer.buffer -> Device2.buffer",
    "ConverterServer.buffer -> Device3.buffer",
    "Device1.buffer -> Mixer.mix1",
    "Device2.buffer -> Mixer.mix2",
    "Device3.buffer -> Mixer.mix3",
    "Mixer.mixed -> ConverterServer.buffer",
]
```