const fs = require('fs')
const nodestatic = require('node-static')
const VRack = require('vrack-core')
const Rule = VRack.Rule
module.exports = class extends VRack.Device {
  checkParams () {
    return [
      new Rule('port').required().default(8080).isInteger().expression('value >= 0'),
      new Rule('path').required().isDirectory()
    ]
  }

  settings () {
    return {
      message: false,
      shares: false,
      storage: false
    }
  }

  preProcess () {
    if (!fs.existsSync(this.params.path)) throw new Error('Static path not found (' + this.params.path + ')')
  }

  process () {
    var file = new nodestatic.Server(this.params.path)
    require('http').createServer(function (request, response) {
      request.addListener('end', function () {
        file.serve(request, response)
      }).resume()
    }).listen(this.params.port)
  }
}
