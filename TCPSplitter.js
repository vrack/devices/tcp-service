const net = require('net')
const { Port, Device } = require('vrack-core')
module.exports = class extends Device {
  outputIDS = ['broadcast', 'receive'];
  inputIDS = [];

  ports () {
    return [
      new Port('broadcast').output().data('Buffer')
        .description('')
    ]
  }

  params = {
    server: {
      host: 'localhost',
      port: 4041
    },
    clients: [{
      host: 'localhost',
      port: 4042
    }]
  };

  settings = {
    messageTypes: ['error', 'notify', 'terminal'],
    storage: false
  }

  shares = {
    broadcastByte: 0,
    receiveByte: 0,
    activeClients: 0
  }

  _server = null;
  _clients = [];
  _serverClient = false;

  process () {
    this.createServer()
    for (const key in this.params.clients) this.createClient(key, this.params.clients[key])

    setInterval(() => {
      this.render()
    }, 5000)
  }

  createServer () {
    this._server = net.createServer((client) => {
      if (this._serverClient) client.end()
      this._serverClient = client
      this._serverClient.on('end', () => {
        this._serverClient = false
      })
      this._serverClient.on('data', (data) => {
        this.terminal('Server got message for broadcast', data)
        this.shares.broadcastByte += data.length
        this.outputs.broadcast.push(data)
        this.serverBroadcast(data)
      })
    })

    this._server.listen(this.params.server, () => {})
  }

  createClient (index, config) {
    this._clients[index] = new net.Socket()

    this._clients[index].on('connect', () => {
      this.notify('TCPSplitter success connected to client', { index: index, config: config })
      this._clients[index].connected = true
      this.shares.activeClients++
    })

    this._clients[index].on('close', () => {
      this.notify('TCPSplitter disconnected from client', { index: index })
      this._clients[index].connected = false
      this.shares.activeClients--
      setTimeout(() => { this._clients[index].connect(config) }, 3000)
    })

    this._clients[index].on('data', (data) => {
      this.terminal('Got message for server from client', data)
      this.shares.receiveByte += data.length
      this.outputs.receive.push(data)
      if (this._serverClient) this._serverClient.write(data)
    })

    this._clients[index].on('error', (error) => {
      this.error(`Client ${index} error`, error)
    })
    this._clients[index].connect(config)
  }

  serverBroadcast (data) {
    for (const key in this._clients) {
      if (this._clients[key].connected) {
        this.terminal('Write data for client index ' + key, data)
        this._clients[key].write(data)
      }
    }
  }
}
