
const WebSocket = require('ws')
const { Rule, Port, Device } = require('vrack-core')
module.exports = class extends Device {
  description = 'Устройство для подключения к ObjectReceiver и отправки объектов/данных'

  ports () {
    return [
      new Port('object').input().data('Object').required()
        .description('Объект отправления')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('127.0.0.1').isString()
        .description('Адрес сервера `ObjectReceiver`'),
      new Rule('port').required().default(8084).isInteger()
        .description('Порт сервера `ObjectReceiver`'),
      new Rule('queueTimeout').required().default(5000).isInteger(),
      new Rule('sendTimeout').required().default(1000).isInteger()
    ]
  }

  settings () {
    return {
      messageTypes: ['error', 'notify'],
      storage: false
    }
  }

  shares = {
    online: false,
    sended: 0,
    noSended: 0
  };

  _buffer = new Map();
  _sended = new Map();
  _queue = new Map();
  _queueTimeout = new Map();
  _pkgIndex = 1;
  _sendTimer = false;
  _ws = false;
  _objectIndex = 1;

  process () {
    this.createClient()
    setInterval(() => {
      this.render()
    }, 5000)
  }

  createClient () {
    this._ws = new WebSocket('ws://' + this.params.host + ':' + this.params.port + '/')

    this._ws.on('error', (error) => {
      this.error('WebSocket Error', error)
    })

    this._ws.on('open', () => {
      this.notify('open')
      this.shares.online = true
      if (this._buffer.size) this.sendTimer()
    })

    this._ws.on('message', (evt) => {
      this.notify('message', JSON.parse(evt))
      const remoteData = JSON.parse(evt)
      if (remoteData._pkgIndex) {
        if (this._queue.has(remoteData._pkgIndex)) {
          clearTimeout(this._queueTimeout.get(remoteData._pkgIndex))
          var func = this._queue.get(remoteData._pkgIndex)
          func(remoteData)
          this._queue.delete(remoteData._pkgIndex)
          this._queueTimeout.delete(remoteData._pkgIndex)
        }
      }
    })

    this._ws.on('close', () => {
      this.notify('close')
      this.shares.online = false
      this.render()
      setTimeout(() => {
        this.createClient()
      }, 5000)
    })
  }

  sendObjects () {
    var data = []
    for (var key of this._buffer.keys()) {
      data.push(this._buffer.get(key))
      this._sended.set(key, true)
    }
    data = {
      objects: data
    }
    var keys = this._sended.keys()
    this.commandPromise(data).then((result) => {
      this.shares.sended += this._sended.size
      for (var key of keys) {
        this._sended.delete(key)
        this._buffer.delete(key)
      }
      this.shares.noSended = this._buffer.size
      this._sendTimer = false
      this.render()
    }).catch((error) => {
      this.error('Error send object', error)
      for (var key of keys) this._sended.delete(key)
      this._ws.terminate()
      this._sendTimer = false
      this.render()
    })
  }

  sendTimer () {
    if (!this.shares.online) return
    if (this._sendTimer) return
    this._sendTimer = setTimeout(() => {
      this._sendTimer = true
      this.render()
      this.sendObjects()
    }, this.params.sendTimeout)
  }

  inputObject (object) {
    if (this._buffer.size > 200) {
      this.sendTimer()
      return
    }
    this._buffer.set(this._objectIndex, object)
    this.shares.noSended = this._buffer.size
    this._objectIndex++
    this.sendTimer()
  }

  commandPromise (data) {
    return new Promise((resolve, reject) => {
      this.command(data, resolve, reject)
    })
  }

  command (data, callback, errorCallback) {
    data._pkgIndex = this._pkgIndex
    this._pkgIndex++
    this.addToQueue(data, callback, errorCallback)
  }

  addToQueue (data, callback, errorCallback) {
    this._queue.set(data._pkgIndex, callback)
    this._queueTimeout.set(data._pkgIndex, setTimeout(() => {
      errorCallback('Timeout')
      this._queue.delete(data._pkgIndex)
      this._queueTimeout.delete(data._pkgIndex)
    }, this.params.queueTimeout))

    if (this.shares.online) {
      this._ws.send(JSON.stringify(data))
    } else {
      errorCallback('WebSocket is close')
    }
  }
}
