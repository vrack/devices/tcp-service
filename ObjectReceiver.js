
const WebSocket = require('ws')
const { Rule, Port, Device } = require('vrack-core')
module.exports = class extends Device {
  ports () {
    return [
      new Port('object').output().data('Object').required()
        .description('Полученый объект')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('0.0.0.0').isString(),
      new Rule('port').required().default(8084).isInteger()
    ]
  }

  settings () {
    return {
      message: false,
      storage: false
    }
  }

  shares = {
    online: true,
    recived: 0
  };

  _ws = false;

  process () {
    const wss = new WebSocket.Server({
      host: this.params.host,
      port: this.params.port
    })
    wss.on('connection', (ws) => {
      ws.on('message', (message) => {
        message = JSON.parse(message)
        this.reciveObjects(ws, message)
      })
    })

    wss.on('error', (error) => {
      this.terminate(error, 'process')
    })

    setInterval(() => {
      this.render()
    }, 5000)
  }

  reciveObjects (ws, message) {
    if (message._pkgIndex) {
      if (!message.objects) return
      if (!message.objects.length) return
      for (var object of message.objects) {
        this.shares.recived++
        this.outputs.object.push(object)
      }
      ws.send(JSON.stringify({
        _pkgIndex: message._pkgIndex
      }))
      this.render()
    }
  }
}
