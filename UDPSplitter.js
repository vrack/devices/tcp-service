const dgram = require('dgram')
const { Device, Rule } = require('vrack-core')
module.exports = class extends Device {
    params = {
      point: {
        address: '127.0.0.1',
        port: 4041,
        newPort: 4041
      },
      server: {
        address: '',
        port: 4042
      },
      clients: [{
        address: '192.168.56.101',
        port: 4043
      }]
    };

    checkParams () {
      return [
        new Rule('point').required().default({
          address: '127.0.0.1',
          port: 4041,
          newPort: 4041
        }).isObject(),
        new Rule('server').required().default({
          address: '',
          port: 4042
        }).isObject(),
        new Rule('clients').required().default([{
          address: '192.168.56.101',
          port: 4043
        }]).isArray()
      ]
    }

    shares = {

    }

    _server = null;
    _renderTimer = false;

    process () {
      this.createServer()
      this._renderTimer = setInterval(() => {
        this.render()
      }, 5000)
    }

    createServer () {
      this._server = dgram.createSocket('udp4')

      this._server.on('error', (error) => {
        this.terminal('server error', error.stack)
      })

      this._server.on('message', (msg, rinfo) => {
        this.terminal(`server got message from ${rinfo.address}:${rinfo.port}`, msg)
        if (rinfo.address === this.params.point.address) {
          this.params.point.newPort = rinfo.port
          for (const key in this.params.clients) {
            this.terminal(key, this.params.clients[key])
            this._server.send(msg, this.params.clients[key].port, this.params.clients[key].address, (err, bytes) => {
              if (err) this.terminal('error send msg', err)
              this.terminal(`UDP message sent to ${this.params.clients[key].address}:${this.params.clients[key].port}`)
            })
          }
        } else {
          this._server.send(msg, this.params.point.newPort, this.params.point.address, (err, bytes) => {
            if (err) this.terminal('error send msg', err)
            this.terminal(`UDP message sent to ${this.params.point.address}:${this.params.point.port}`)
          })
        }
      })

      this._server.on('listening', (error) => {
        this.error('Listening', error)
        const address = this._server.address()
        this.terminal(`server listening ${address.address}:${address.port}`)
      })

      this._server.bind(this.params.server.port, this.params.server.address)
    }
}
