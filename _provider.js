const net = require('net')
const EventEmitter = require('events')
module.exports = class extends EventEmitter {
    state = {
      counter: 0,
      timeout: false,
      connected: false,
      progress: false,
      connection: false,
      errors: 0,
      byteSend: 0,
      byteReceive: 0
    }

    #ready = function () { };

    #options = {};

    #socket = {};
    #buffer = Buffer.from('');
    #pkgCheck = () => { return true };
    #destroy = () => { };
    #timeoutTimer = {};
    #resolve = false
    #reject = false

    getBuffer () {
      return this.#buffer
    }

    constructor (options, device) {
      super()
      this.#options = options
      this.device = device
      this.createSocket()
    }

    createSocket () {
      this.#socket = new net.Socket()
      this.#socket.setTimeout(this.#options.timeout)
      this.state.timeout = false
      this.#socket.on('connect', () => {
        this.state.timeout = false
        this.state.connection = false
        this.state.connected = true
        this.emit('connected', 1)
        this.#ready()
      })

      this.#socket.on('timeout', () => {
        this.state.timeout = true
        this.#socket.destroy(new Error('Socket connection timeout'))
        setTimeout(() => {
          this.createSocket()
        }, 1000)
      })

      this.#socket.on('error', (error) => { this.emit('connected', error) })
      this.#socket.on('close', () => {
        this.#destroy()
        this.state.connected = false
        this.state.connection = false
        this.emit('connected', 0)
        if (!this.state.timeout) { setTimeout(() => { this.#socket.connect(this.#options) }, 3000) }
      })
      this.state.connection = true
      this.#socket.connect(this.#options)
    }

    setDestroy (destroy) { this.#destroy = destroy }
    setReady (rdy) { this.#ready = rdy }
    setPkgCheck (cb) { this.#pkgCheck = cb }
    async autoRequest (buffer, timeout, count = 3) {
      var start = (new Date().getTime())
      for (let i = 1; i <= count; i++) {
        try {
          await this.request(buffer, timeout)
          var end = (new Date().getTime())
          this.emit('request', end - start)
          return true
        } catch (error) {
          end = (new Date().getTime())
          this.emit('timeout', end - start)
          if (i === count) {
            throw new Error(`Requests (${count}) timeout`)
          }
        }
      }
    }

    async request (buffer, timeout) {
      if (this.state.progress) throw new Error('Provider is busy')
      if (!this.state.connected) throw new Error('Provider not connected')
      this.state.progress = true
      this.#buffer = Buffer.from('')

      return new Promise((resolve, reject) => {
        this.#resolve = resolve
        this.#reject = reject
        this.#socket.once('data', this._once.bind(this))
        this.#socket.write(buffer)
        this.state.byteSend += buffer.length
        this.#timeoutTimer = setTimeout(() => {
          this.#socket.removeAllListeners('data')
          this.state.progress = false
          this.state.errors++
          reject(new Error('Request timeout'))
        }, timeout)
      })
    }

    _once (data) {
      this.state.byteReceive += data.length
      this.#buffer = Buffer.concat([this.#buffer, data])
      if (this.#pkgCheck(this.#buffer)) {
        clearTimeout(this.#timeoutTimer)
        this.state.progress = false
        this.state.counter++
        this.#resolve(true)
      } else {
        this.#socket.once('data', this._once.bind(this))
      }
    }
}
