const { Rule, Port, Device } = require('vrack-core')
const VRackError = require('vrack-core').Error
const http = require('http')
module.exports = class extends Device {
  description = 'Принимает JSON зарпосы и формирует и возвращает результат';

  ports () {
    return [
      new Port('data').output().data('Object').modeReturn().description('Данные запроса')
    ]
  }

  checkParams () {
    return [
      new Rule('port').default(8099).isInteger().expression('value > 0')
        .description('Порт сервера')
    ]
  }

  settings () {
    return {
      message: true,
      shares: true,
      messageTypes: ['error', 'event']
    }
  }

  shares = {
    requests: 0,
    errors: 0
  }

  process () {
    const server = http.createServer((req, res) => {
      var jsonString = ''
      res.setHeader('Content-Type', 'application/json')
      req.on('data', (data) => { jsonString += data })
      req.on('end', async () => {
        await this.ApiResponse(jsonString, res)
      })
    })
    server.on('clientError', (err, socket) => {
      this.error('Client Error', err)
      socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
    })
    server.listen(this.params.port)
  }

  async ApiResponse (jsonString, res) {
    this.event('Http input buffer', jsonString)
    var response = {}
    try {
      const command = JSON.parse(jsonString)
      this.event('API input command', command)
      const res = await this.outputs.command.push(command)
      if (typeof res === 'object') response = res
      this.event('API command result', res)
      this.shares.requests++
    } catch (error) {
      this.shares.errors++
      const data = VRackError.objectify(error)
      response = data
    }
    res.writeHead(200, { 'Content-Type': 'application/json' })
    res.write(JSON.stringify(response))
    res.end()
    this.render()
  }
}
