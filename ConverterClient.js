const TCPProvider = require('./_provider')
const VRack = require('vrack-core')
const Rule = VRack.Rule
const Port = VRack.Port

module.exports = class extends VRack.Device {
  ports () {
    return [
      new Port('gate').input().data('signal').required()
        .description('Вход передачи управления провадером'),
      new Port('provider').output().data('data').required()
        .description('Выход провайдера'),
      new Port('metric.connected').output().data('number').required()
        .description('Состояние соединения с преобразователем'),
      new Port('metric.request').output().data('number').required()
        .description('Время потраченое на успешный запрос'),
      new Port('metric.timeout').output().data('number').required()
        .description('Время потраченное на неотвеченый запрос')
    ]
  }

  checkParams () {
    return [
      new Rule('host').required().default('localhost').isString(),
      new Rule('port').required().default(4001).isInteger().expression('value >= 0'),
      new Rule('timeout').required().default(15000).isInteger().expression('value >= 0')
    ]
  }

  settings () {
    return {
      message: false,
      shares: true
    }
  }

  shares = {}

  #provider = {};
  #inputGate = true;

  #metricList = [
    'connected',
    'request',
    'timeout'
  ]

  process () {
    this.#provider = new TCPProvider(this.params, this)
    this.#provider.setReady(this.ready.bind(this))
    for (const metric of this.#metricList) {
      this.#provider.on(metric, (status) => {
        this.outputs['metric.' + metric].push(status)
        this.render()
      })
    }
    this.shares = this.#provider.state
  }

  ready () {
    if (this.#inputGate) { this.inputGate() }
  }

  inputGate () {
    this.#inputGate = true
    if (this.#provider.state.connected && this.#provider.state.progress === false) {
      this.#inputGate = false
      setTimeout(() => { this.outputs.provider.push(this.#provider) }, 50)
    }
  }
}
